@extends('layout')


@section('content')
    <form action="{{ action('AppController@store') }}" method="POST" class="form-horizontal" id="myForm"
          class="myForm">
        {!! csrf_field() !!}
        <div class="step1">
            <div class="logo">
                <img src="{{ url ('images/logo.png') }}" alt="Zitouna">
            </div>
            <div class="title">
                <h3>Simulateur de Financement</h3>
            </div>
            <div class="financement">
                <h4>Type de Financement</h4>
                            <span class="selectbox">
                                <select name="financement" id="financement" onchange="choixFinancement();"
                                        class="required" required="" data-parsley-group="block1">
                                    <option value="Tamouil_Sayara">Tamouil Sayara</option>
                                    <option value="Tamouil_Menzel">Tamouil Menzel</option>
                                    <option value="Tamouil_Akkarat_Afrad">Tamouil Akkarat El Afrad</option>
                                    <option value="Tamouil_moueddet_Ennakl">Tamouil Moueddet Ennakl</option>
                                    <option value="Tamouil_Mochtarayet">Tamouil Mochtarayet</option>
                                </select>
                            </span>
            </div>
            <div class="prix">
                <img src="{{ url('images/icon1.png') }}">
                <h4>Montant</h4>
                <input type="text" name="prix" id="prix" class="" onkeyup="onChangePrice()"
                       value="{{ Request::get('prix')  }}" required="" data-parsley-group="block1">
                <h4 class="dt">DT</h4>
            </div>
            <div class="clearfix"></div>
            <div id="ageId" class="age">
                <img src="{{ url('images/icon2.png') }}">
                <h4>Age de la voiture (ans)</h4>
                <span><input type="radio" name="age" value="0" id="radio1" class="css-checkbox"
                             onchange="onChangeAge(this)" <?php if (date("Y") - Request::get('model') == 0) echo 'checked'; ?>><label
                            for="radio1" class="css-label">0</label></span>
                <span><input type="radio" name="age" value="1" id="radio2" class="css-checkbox"
                             onchange="onChangeAge(this)"
                             <?php if (date("Y") - Request::get('model') == 1) echo 'checked'; ?> checked><label for="radio2"
                                                                                                   class="css-label">1</label></span>
                <span><input type="radio" name="age" value="2" id="radio3" class="css-checkbox"
                             onchange="onChangeAge(this)" <?php if (date("Y") - Request::get('model') == 2) echo 'checked'; ?>><label
                            for="radio3" class="css-label">2</label></span>
                <span><input type="radio" name="age" value="3" id="radio4" class="css-checkbox"
                             onchange="onChangeAge(this)" <?php if (date("Y") - Request::get('model') >= 3) echo 'checked'; ?>><label
                            for="radio4" class="css-label">3</label></span>
            </div>
            <div class="clearfix"></div>
            <div id="puissanceFiscaleId" class="puissance">
                <img src="{{ url('images/icon3.png') }}">
                <h4>Puissance Fiscale</h4>
                <span><input type="radio" name="puissance_fiscale" value="4" id="radio5" class="css-checkbox"
                             onchange="onChangeNombreCheveaux(20)" <?php if (Request::get('puissance_fiscale') == 4) echo 'checked'; ?> checked><label
                            for="radio5" class="css-label">4CV</label></span>
                <span><input type="radio" name="puissance_fiscale" value="5" id="radio6" class="css-checkbox"
                             onchange="onChangeNombreCheveaux(40)" <?php if (Request::get('puissance_fiscale') == 5) echo 'checked'; ?>><label
                            for="radio6" class="css-label">5 à 8CV</label></span>
                <span><input type="radio" name="puissance_fiscale" value="9" id="radio7" class="css-checkbox"
                             onchange="onChangeNombreCheveaux(70)" <?php if (Request::get('puissance_fiscale') == 9) echo 'checked'; ?>><label
                            for="radio7" class="css-label">9CV ou plus</label></span>
            </div>
            <div class="clearfix"></div>
            <div id="dureeId" class="duree">
                <img src="{{ url('images/icon4.png') }}">
                <h4>Durée</h4>
                <input id="dureeslider" type="text" name="duree" data-provide="slider" onchange="footer_value()">
            </div>
            <div class="autofinancement">
                <img src="{{ url('images/icon5.png') }}">
                <h4>Autofinancement</h4>
                <input type="text" name="auto_financement" id="auto_financement" onchange="setAutofinancement()"
                       class="required" required="" data-parsley-group="block1">
                <h4 class="dt">DT</h4>
            </div>
            <div class="clearfix"></div>

            <div class="footer-wrapper">
                <div class="mensualiteClass">
                    <h3><i class="fa fa-play" aria-hidden="true"></i>
                        Mensualité : <span id="mensualite_step3">-</span></h3>
                </div>
                <div class="text">
                    <h4> Montant Emprunté : <span id="montant_2" class="montantEmp">-</span></h4>
                    <h4> Durée : <span id="durespan_2" class="duree">-</span></h4>
                </div>

                <div class="btn-block">
                    <button type="button" class="btn_calc" onclick="switchtab(1)"> Contactez-moi</button>
                </div>
            </div>

        </div>
        <div class="step2 hide">
            <div class="logo">
                <img src="{{ url ('images/logo.png') }}" alt="Zitouna">
            </div>
            <div class="title">
                <h4>Veuillez remplir le formulaire</h4>
                <div class="line"></div>
            </div>
            <div class="name">
                <img src="{{ url('images/icon6.png') }}">
                <h4>Nom :</h4>
                <input type="text" name="name" id="name" class="form-control required" required=""
                       data-parsley-group="block2" title="Nom incorrect" placeholder="Nom *">
            </div>
            <div class="clearfix"></div>
            <div class="surname">
                <img src="{{ url('images/icon6.png') }}">
                <h4>Prénom :</h4>
                <input type="text" name="surname" id="surname" class="form-control required" required=""
                       data-parsley-group="block2" title="Prénom incorrect" placeholder="Prénom *">
            </div>
            <div class="clearfix"></div>
            <div class="email">
                <img src="{{ url('images/icon7.png') }}">
                <h4>Email :</h4>
                <input type="text" name="email" id="email" class="form-control required" data-parsley-type="email"
                       placeholder="Email *" title="Email incorrect" required="" data-parsley-group="block2">
            </div>
            <div class="clearfix"></div>
            <div class="telephone">
                <img src="{{ url('images/icon8.png') }}">
                <h4>Téléphone :</h4>
                <input type="text" name="telephone" id="telephone" class="form-control required" maxlength="8"
                       placeholder="Téléphone *" maxlength="8" required="" data-parsley-group="block2"
                       pattern="^[0-9]{8}$" title="Téléphone incorrect">
            </div>
            <div class="clearfix"></div>
            <div class="divBTN">
                <button type="button" class="validate-btn loading" data-loading-text="Patientez..."
                        onclick="switchtab(2)"> Valider
                </button>
            </div>
            <div class="clearfix"></div>
            <div class="footer-wrapper">
                <div class="mensualiteClass">
                    <h3><i class="fa fa-play" aria-hidden="true"></i>
                        Mensualité : <span id="mensualite_step3">-</span></h3>
                </div>
                <div class="text">
                    <h4> Montant Emprunté : <span id="montant_2" class="montantEmp">-</span></h4>
                    <h4> Durée : <span id="durespan_2" class="duree">-</span></h4>
                </div>

                <div class="btn-block">
                    <button type="button" class="btn_calc" onclick="switchtab(0)"> Retour</button>
                </div>

            </div>
        </div>
        <div class="step3 hide">
            <div class="logo">
                <img src="{{ url ('images/logo.png') }}" alt="Zitouna">
            </div>
            <div class="title">
                <h3 class="message-merci">Merci pour votre intérêt</h3>
                <h5 class="message-contact">Nous vous contacterons dans les plus brefs délais.</h5>
                <div class="line remerciment"></div>
            </div>
            <div class="divBTN">
                <button type="button" class="return-btn" onclick="showPage('.step1')"> Refaire une simulation</button>
            </div>
            <div class="clearfix"></div>
            <div class="footer-wrapper">
                <div class="mensualiteClass">
                    <h3><i class="fa fa-play" aria-hidden="true"></i>
                        Mensualité : <span id="mensualite_step3">-</span></h3>
                </div>
                <div class="text">
                    <h4> Montant Emprunté : <span id="montant_2" class="montantEmp">-</span></h4>
                    <h4> Durée : <span id="durespan_2" class="duree">-</span></h4>
                </div>
            </div>
        </div>
    </form>
@endsection
@push('scripts')
    <script type="text/javascript">

        $('#prix').autoNumeric();
        $('#auto_financement').autoNumeric();

        function convertir_mensualité(valeur) {
            var retour = String(valeur);
            if (retour.indexOf('.') == -1) {
                var str = convertir_nombre(retour);
                return str + '.000';
            }
            var reel = retour.substr(retour.indexOf('.'), retour.length - 1);
            var entier = retour.substr(0, retour.indexOf('.'));
            return convertir_nombre(entier) + reel;
        }

        function convertir_nombre(nbr) {
            var nombre = '' + nbr;
            var retour = '';
            var count = 0;
            for (var i = nombre.length - 1; i >= 0; i--) {
                if (count != 0 && count % 3 == 0 && nombre.charAt(count) != '.')
                    retour = nombre[i] + ' ' + retour
                else
                    retour = nombre[i] + retour

                count++;
            }
            return retour;
        }

        function reverse_nombre(chaine) {
            var nombre = chaine.replace(/\s/g, '');
            return parseInt(nombre);
        }

        function switchtab(index) {
            if (index == 0) {
                showPage('.step1');
            }
            if (index == 1) {
                if ($('#myForm').parsley().validate({group: 'block1'}))
                    showPage('.step2');
            }

            if (index == 2) {
                if ($('#myForm').parsley().validate({group: 'block2'})) {
                    $('.loading').button('loading');
                    var $this = $('#myForm');
                    $.ajax({
                        url: $this.attr('action'),
                        method: 'POST',
                        data: $this.serialize()
                    }).done(function (response) {
                        $('.loading').button('reset');
                        if (response.success) {
                            showPage('.step3');
                        } else {
                            showPage('.step1');
                        }
                    });
                }
            }
        }

        function showPage(selector) {
            $(selector).removeClass('hide').siblings().addClass('hide');
        }

        $(document).ready(function () {

            $('#myForm').parsley({
                errorsContainer: function (ParsleyField) {
                    return ParsleyField.$element.attr("title");
                },
                errorsWrapper: false
            });
            $.listen('parsley:field:error', function (fieldInstance) {
                var messages = ParsleyUI.getErrorsMessages(fieldInstance);
                fieldInstance.$element.tooltip('destroy');
                fieldInstance.$element.tooltip({
                    animation: true,
                    container: 'body',
                    placement: 'top',
                    title: messages
                });
            });
            $.listen('parsley:field:success', function (fieldInstance) {
                fieldInstance.$element.tooltip('destroy');
            });
        });

        // function return requests parameter
        function getRequests() {
            var s1 = location.search.substring(1, location.search.length).split('&'),
                    r = {}, s2, i;
            for (i = 0; i < s1.length; i += 1) {
                s2 = s1[i].split('=');
                r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
            }
            return r;
        }
        var requestParams = getRequests();


        var data = '{"data_type":"json","typeFinancement":[' +
                '{"type":"Tamouil_Sayara",' +
                '"values":[' +
                '{"name":"default","plafond":100000,"Apport_propre":20,"dureeMin":1,"dureeMax":7},' +
                '{"name":"voiture","cv":4,"plafond":100000,"Apport_propre":20,"dureeMin":1,"dureeMax":7},' +
                '{"name":"voiture","cv":5,"plafond":100000,"Apport_propre":40,"dureeMin":1,"dureeMax":7},' +
                '{"name":"voiture","cv":9,"plafond":100000,"Apport_propre":70,"dureeMin":1,"dureeMax":7},' +
                '{"name":"motos","cv":4,"plafond":100000,"Apport_propre":20,"dureeMin":1,"dureeMax":7},' +
                '{"name":"motos","cv":5,"plafond":100000,"Apport_propre":40,"dureeMin":1,"dureeMax":7},' +
                '{"name":"motos","cv":9,"plafond":100000,"Apport_propre":70,"dureeMin":1,"dureeMax":7}' +
                '],' +
                '"marge":{"1":"4.52","2":"4.41","3":"4.41","4":"4.58","5":"4.62","6":"4.82","7":"4.87"}},' +
                '{"type":"Tamouil_moueddet_Ennakl",' +
                '"values":[' +
                '{"name":"default","plafond":"0","Apport_propre":40,"dureeMin":1,"dureeMax":7},' +
                '{"name":"Tracteurs-véhicules-agricoles","plafond":"0","Apport_propre":40,"dureeMin":1,"dureeMax":7},' +
                '{"name":"Fourgonettes","plafond":"0","Apport_propre":40,"dureeMin":1,"dureeMax":7},' +
                '{"name":"Camions-véhicules-industriels","plafond":"0","Apport_propre":40,"dureeMin":1,"dureeMax":7}' +
                '],' +
                '"marge":{"1":"4.61","2":"4.49","3":"4.49","4":"4.66","5":"4.71","6":"4.90","7":"4.96"}},' +
                '{"type":"Tamouil_Mochtarayet",' +
                '"values":[' +
                '{"name":"default","plafond":30000,"Apport_propre":0,"dureeMin":1,"dureeMax":3},' +
                '{"name":"Motos-sans-Carte-Grise","plafond":30000,"Apport_propre":0,"dureeMin":1,"dureeMax":3}' +
                '],' +
                '"marge":{"1":"4.80","2":"4.68","3":"4.69"}},' +
                '{"type":"standard",' +
                '"values":[' +
                '{"name":"Bateaux"},' +
                '{"name":"Autres-véhicules"},' +
                '{"name":"Pièces-rechange-accessoires"},' +
                '{"name":"Colocations"}' +
                ']},' +
                '{"type":"Tamouil_Menzel",' +
                '"values":[' +
                '{"name":"default","plafond":"0","Apport_propre":20,"dureeMin":5,"dureeMax":20},' +
                '{"name":"Appartement","plafond":"0","Apport_propre":20,"dureeMin":5,"dureeMax":20},' +
                '{"name":"Maison","plafond":"0","Apport_propre":20,"dureeMin":5,"dureeMax":20},' +
                '{"name":"Immobilier-de-vacances","plafond":"0","Apport_propre":20,"dureeMin":5,"dureeMax":20},' +
                '{"name":"terrains","plafond":"0","Apport_propre":20,"dureeMin":5,"dureeMax":20}' +
                '],' +
                '"marge":{"5":"4.19","6":"4.37","7":"4.42","8":"4.62","9":"4.67","10":"4.72","11":"4.93","12":"4.99","13":"5.04","14":"5.10","15":"5.15","16":"5.38","17":"5.44","18":"5.50","19":"5.55","20":"5.60"}},' +
                '{"type":"Tamouil_Akkarat_Afrad",' +
                '"values":[' +
                '{"name":"default","plafond":"0","Apport_propre":30,"dureeMin":2,"dureeMax":7},' +
                '{"name":"Bureaux-locaux-commerciaux","plafond":"0","Apport_propre":30,"dureeMin":2,"dureeMax":7},' +
                '{"name":"Autre-Immobilier","plafond":"0","Apport_propre":30,"dureeMin":2,"dureeMax":7}' +
                '],' +
                '"marge":{"2":"4.55","3":"4.55","4":"4.58","5":"4.62","6":"4.67","7":"4.72"}}' +
                ']}';
        var jsonData = JSON.parse(data);
        var plafond = 0;

        //initialisation de formulaire dé le debut
        (function () {

            var DureeMin = 0;
            var DureeMax = 0;
            var apport = 0;
            var valeur = 0;
            var apportPropre = 1;

            //initialiser les champs de simulateur selon le type de financement
            for (var i = 0; i < jsonData.typeFinancement.length; i++) {
                for (var j = 0; j < jsonData.typeFinancement[i].values.length; j++) {
                    if (requestParams["categorie"] == null) {
                        choixFinancement();
                    } else {
                        if (requestParams["categorie"].toLowerCase() == jsonData.typeFinancement[i].values[j].name.toLowerCase()) {
                            if (jsonData.typeFinancement[i].type != 'Tamouil_Sayara') {
                                $("#ageId").hide();
                                $("#puissanceFiscaleId").hide();
                            }
                            $("#financement").val(jsonData.typeFinancement[i].type);
                            DureeMin = parseInt(jsonData.typeFinancement[i].values[j].dureeMin);
                            DureeMax = parseInt(jsonData.typeFinancement[i].values[j].dureeMax);
                        }
                    }
                }
            }

            /*
            var CurrentYear = new Date().getFullYear();
            var model = parseInt(requestParams["model"]);
            var age = CurrentYear - model;
            console.log('difference between current year & request age = ',age);
            var warning = '<div class="alert alert-warning alert-dismissible"><a class="close" data-dismiss="alert">×</a><span>OUPPS !!</span></div>';
            if ( age > 3 ){
                $('<div class="alert alert-warning alert-dismissible"><a class="close" data-dismiss="alert">×</a><span>Notre simulateur l\'age de la voiture depasse</span></div>').appendTo("body");
            }
            */

            // remplir le tableau duree pour le slider
            var array_thick = [];
            var index = 0;
            var age = $('.css-checkbox:checked').val();
            for (var k = DureeMin; k <= DureeMax; k++) {
                array_thick[index] = k;
                index++;
            }
            $("#dureeslider").slider({
                ticks: array_thick,
                ticks_labels: array_thick,
                min: 1,
                max: array_thick.length,
                step: 1,
                value: array_thick.length
            });

            getplafond($("#financement").val());
            footer_value();
        })();

        function calculer_mensualite() {

            var montant_financement = reverse_nombre($("#prix").val()) - reverse_nombre($("#auto_financement").val());

            console.log('montant_financement = ',montant_financement);

            var marge_globale = (marge_profit_annualise($("#financement").val()) / 100) * parseInt($("#dureeslider").val());
            var mensualitePart1 = montant_financement * ( 1 + marge_globale );
            var mensualitePart2 = parseInt($("#dureeslider").val()) * 12;

            var mensualite = mensualitePart1 / mensualitePart2;
            console.log('mensualite = ',mensualite);

            return convertir_mensualité(parseFloat(mensualite).toFixed(3));
        }

        function marge_profit_annualise(param) {
            var marge_index = parseInt($("#dureeslider").val());
            for (var i = 0; i < jsonData.typeFinancement.length; i++) {
                if (param == jsonData.typeFinancement[i].type) {
                    var marge = jsonData.typeFinancement[i].marge[marge_index];
                }
            }
            return parseFloat(marge);
        }

        function getplafond(categorie) {
            for (var i = 0; i < jsonData.typeFinancement.length; i++) {
                if (jsonData.typeFinancement[i].type == categorie) {
                    plafond = jsonData.typeFinancement[i].values[0].plafond;
                    apportPropre = jsonData.typeFinancement[i].values[0].Apport_propre;
                    /*$("#auto_financement").val((apportPropre * plafond) / 100);
                    if (plafond != 0) {
                        //$('#prix').val(plafond);
                    } else {
                        //var prix = reverse_nombre($('#prix').val());
                        //$("#auto_financement").val((apportPropre * prix) / 100);
                    }*/
                    var prix = reverse_nombre($('#prix').val());
                    $("#auto_financement").val((apportPropre * prix) / 100);
                }
            }
        }

        function getApportPropre(categorie) {
            var tempPropre;
            for (var i = 0; i < jsonData.typeFinancement.length; i++) {
                if (jsonData.typeFinancement[i].type == categorie) {
                    tempPropre = jsonData.typeFinancement[i].values[0].Apport_propre;
                }
            }
            return parseInt(tempPropre);
        }

        function onChangeAge(param) {

            var min = param.value;
            var array_thick2 = [];
            var array_thick_labels = [];
            var index2 = 0;
            for (var k = 1; k < 7 - min + 1; k++) {
                array_thick2[index2] = k;
                array_thick_labels[index2] = k + ' ans';
                index2++;
            }
            $("#dureeslider").slider('destroy');
            $("#dureeslider").slider({
                ticks: array_thick2,
                ticks_labels: array_thick2,
                min: 1,
                max: array_thick2.length,
                step: 1,
                value: array_thick2.length
            });
            $("#dureeslider").slider('refresh');
        }

        function select_age_checked() {
            var radios = document.getElementsByName('age');
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].checked) {
                    return radios[i];
                }
            }
        }

        function choixFinancement() {

            var financement = $("#financement").val();
            $('#prix').val(reverse_nombre($('#prix').val()));
            $('#auto_financement').val(reverse_nombre($('#auto_financement').val()));
            getplafond(financement);
            $('#prix').val(convertir_nombre($('#prix').val()));
            $('#auto_financement').val(convertir_nombre($('#auto_financement').val()));

            if (financement == 0) {
                $("#ageId").hide();
                $(".prix").hide();
                $("#dureeId").hide();
                $('#auto_financement').hide();
                $(".footer-wrapper").hide();
            }
            if (financement != 'Tamouil_Sayara') {
                $("#ageId").hide();
                $("#puissanceFiscaleId").hide();
            } else {
                $("#ageId").show();
                $("#puissanceFiscaleId").show();

                var checked_age = select_age_checked();
                onChangeAge(checked_age);
                return;
            }

            for (var i = 0; i < jsonData.typeFinancement.length; i++) {
                if (financement == jsonData.typeFinancement[i].type) {
                    var default_value = jsonData.typeFinancement[i].values[0];
                    var min = default_value["dureeMin"];
                    var max = default_value["dureeMax"];
                    var array_thick2 = [];
                    var array_thick_labels = [];
                    var index2 = 0;
                    for (var k = min; k < max + 1; k++) {
                        array_thick2[index2] = k;
                        array_thick_labels[index2] = k + ' ans';
                        index2++;
                    }
                    $("#dureeslider").slider('destroy');
                    $("#dureeslider").slider({
                        ticks: array_thick2,
                        ticks_labels: array_thick2,
                        min: 1,
                        max: array_thick2.length,
                        step: 1,
                        value: array_thick2.length
                    });
                }
            }
        }

        /* fonction qui test la valeur ajouté par l'utilisateur par rapport a la valeur exigé  */
        function setAutofinancement() {
            var nouvelleApport = reverse_nombre($("#auto_financement").val());
            var prix = reverse_nombre($("#prix").val());
            if ($("#financement").val() == 'Tamouil_Sayara') {
                if ($('#radio5').is(':checked')) {
                    apportPropre = 20
                }
                if ($('#radio6').is(':checked')) {
                    apportPropre = 40
                }
                if ($('#radio7').is(':checked')) {
                    apportPropre = 70
                }
            } else {
                apportPropre = getApportPropre($("#financement").val());
            }

            var valeur = (prix * apportPropre) / 100;

            var auto_financement = 0;
            if (nouvelleApport > 0) {
                if (nouvelleApport < valeur) {
                    auto_financement = valeur;
                }
                if (nouvelleApport > valeur) {
                    auto_financement = nouvelleApport;
                }
                if (nouvelleApport >= prix) {
                    auto_financement = ((apportPropre * prix) / 100);
                }
            }

            $("#auto_financement").val(convertir_nombre(auto_financement));
            footer_value();
        }

        function onChangeNombreCheveaux(apport) {
            apportPropre = apport;
            var prix = reverse_nombre($("#prix").val());
            var valeur = ( apport * prix ) / 100;
            $("#auto_financement").val(valeur);
            $("#auto_financement").val(convertir_nombre($("#auto_financement").val()));
            footer_value();
        }

        function onChangePrice() {

            var nouveauPrix = reverse_nombre($("#prix").val());

            if (nouveauPrix == "") {
                $("#prix").val(0);
            }
            if (parseInt(nouveauPrix) < 0) {
                $("#prix").val(Math.abs(nouveauPrix));
                nouveauPrix = Math.abs(nouveauPrix);
            }

            if (String(nouveauPrix).length > 9) {
                nouveauPrix = parseInt(String(nouveauPrix).substr(0, 9));
                $("#prix").val(convertir_nombre(nouveauPrix));
            }

            footer_value();
            setAutofinancement();
            if ($("#financement").val() == 'Tamouil_Sayara') {
                if ( $('#radio5').is(':checked') && ( reverse_nombre($('#prix').val()) <= plafond)) {
                    onChangeNombreCheveaux(20);
                }
                if ($('#radio6').is(':checked') && ( reverse_nombre($('#prix').val()) <= plafond)) {
                    onChangeNombreCheveaux(40);
                }
                if ($('#radio7').is(':checked') && ( reverse_nombre($('#prix').val()) <= plafond)) {
                    onChangeNombreCheveaux(70);
                }
            }
        }

        function footer_value() {
            $('.footer-wrapper .mensualiteClass span').html(calculer_mensualite() + ' dt');
            $('.footer-wrapper .duree').html($("#dureeslider").slider('getValue') + ' ans');
            if (plafond != 0) {
                var montant_emprunte = reverse_nombre($("#prix").val()) - reverse_nombre($("#auto_financement").val());
                if (montant_emprunte > plafond) {
                    var auto_financement = reverse_nombre($("#auto_financement").val());
                    auto_financement = auto_financement + (montant_emprunte - plafond);
                    $("#auto_financement").val(convertir_nombre(auto_financement));
                    montant_emprunte = plafond;
                }
            }
            var montant_emprunte = reverse_nombre($("#prix").val()) - reverse_nombre($("#auto_financement").val());
            $('.footer-wrapper .montantEmp').html(convertir_nombre(montant_emprunte) + ' dt');
        }

        $('#prix').val(convertir_nombre($('#prix').val()));
        $('#auto_financement').val(convertir_nombre($('#auto_financement').val()));
        $('#prix').autoNumeric();
        $('#auto_financement').autoNumeric();
        $("#dureeslider").slider('refresh');

    </script>
@endpush