@extends('admin.layouts.blank')

@section('main_container')
    <div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Statistique</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="">
                    <div class="x_content">
                        <div class="row">
                            <a href="{{ url("admin/tamouil_sayara") }}">
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="tile-stats">
                                    <div class="icon"><i class="fa fa-car"></i>
                                    </div>
                                    <div class="count">{{ $nbsayara }}</div>
                                    <h3>Financement Sayyara</h3>
                                    <!--<p>Tamouil Sayyara</p>-->
                                </div>
                            </div>
                            </a>
                            <a href="{{ url("admin/tamouil_moueddet_ennakl") }}">
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="tile-stats">
                                    <div class="icon"><i class="fa fa-truck"></i>
                                    </div>
                                    <div class="count">{{ $nbennakl }}</div>
                                    <h3>Tamouil Moueddet Ennakl</h3>
                                <!--<p>Tamouil Moueddet Ennakl</p>-->
                                </div>
                            </div>
                            </a>
                            <a href="{{ url("admin/tamouil_mochtarayet") }}">
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="tile-stats">
                                    <div class="icon"><i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="count">{{ $nbmochtarayet }}</div>
                                    <h3>Tamouil Mochtarayet</h3>
                                    <!--<p>Tamouil Mochtarayet</p>-->

                                </div>
                            </div>
                            </a>
                            <a href="{{ url("admin/tamouil_menzel") }}">
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="tile-stats">
                                    <div class="icon"><i class="fa fa-home"></i>
                                    </div>
                                    <div class="count">{{ $nbmanzel }}</div>
                                    <h3>Tamouil Menzel</h3>
                                    <!--<p>Tamouil Menzel</p>-->
                                </div>
                            </div>
                            </a>
                            <a href="{{ url("admin/tamouil_akkarat_afrad") }}">
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="tile-stats">
                                    <div class="icon"><i class="fa fa-building"></i>
                                    </div>
                                    <div class="count">{{ $nbakkarat }}</div>
                                    <h3>Tamouil Akkarat Afrad</h3>
                                    <!--<p>Tamouil Akkarat Afrad</p>-->
                                </div>
                            </div>
                            </a>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Financement par type</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                        <div class="col-md-8 col-lg-8 col-sm-7 tile">
                                            <!-- blockquote -->
                                            <span>Nombre De Financement Totale :</span>
                                            <h2>{{ $total }}</h2>
                                            <span class="sparkline_one" style="height: 160px;">
                                                <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                            </span>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-5">
                                            <h3>Financement En % :</h3>
                                            <h4><strong> Tamouil Sayara :</strong> {{ number_format($sayara, 2) }} %</h4>
                                            <h4><strong> Tamouil Moueddet Ennakl :</strong> {{ number_format($ennakl, 2) }} %</h4>
                                            <h4><strong> Tamouil Mochtarayet :</strong> {{ number_format($mochtarayet, 2) }} %</h4>
                                            <h4><strong> Tamouil Menzel :</strong> {{ number_format($menzel, 2) }} %</h4>
                                            <h4><strong> Tamouil Akkarat Afrad :</strong> {{ number_format($akkarat, 2) }} %</h4>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <canvas id="myChart" width="400" height="400"></canvas>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-lg-12">
                                            <h1>Financement par montant</h1>
                                            <p>Chiffre totale de financement : {{ $montant }}</p>
                                            <p>Le pourcentage de financement: </p>
                                            <ul>
                                                <li><strong> Tamouil Sayara :</strong> {{ number_format($plage1, 2) }} %</li>
                                                <li><strong> Tamouil Moueddet Ennakl :</strong> {{ number_format($plage2, 2) }} % </li>
                                                <li><strong> Tamouil Mochtarayet :</strong> {{ number_format($plage3, 2) }} %</li>
                                                <li><strong> Tamouil Menzel :</strong> {{ number_format($plage4, 2) }} %</li>
                                                <li><strong> Tamouil Akkarat Afrad :</strong> {{ number_format($plage5, 2) }} % </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
        type: 'bar',
        height: '40',
        barWidth: 9,
        colorMap: {
            '7': '#a1a1a1'
        },
        barSpacing: 2,
        barColor: '#26B99A'
    });

    var data = {
        labels: [
            "Tamouil Sayara",
            "Tamouil Moueddet Ennakl",
            "Tamouil Mochtarayet",
            "Tamouil Menzel",
            "Tamouil Akkarat Afrad"
        ],
        datasets: [
            {
                data: [{{ $nbsayara }}, {{ $nbennakl }}, {{ $nbmochtarayet }}, {{ $nbmanzel }}, {{ $nbakkarat }}],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56",
                    "#D0288D",
                    "#A0728D"
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56",
                    "#D0288D",
                    "#A0728D"
                ]
            }]
    };

    var ctx = document.getElementById("myChart");

    var myPieChart = new Chart(ctx,{
        type: 'pie',
        data: data
    });
</script>
@endpush

