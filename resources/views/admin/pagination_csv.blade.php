@extends('admin.layouts.blank')

@section('main_container')

        <!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="col-sm-12">
            <h1>Liste Des  Membres</h1>
        </div>
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="col-md-6">
                            <h2 class="pull-left">Liste participants</h2>
                        </div>
                        <div class="col-md-6">
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th align="center">Id</th>
                                <th align="center">Nom et Prénom</th>
                                <th align="center">Email</th>
                                <th align="center">Téléphone</th>
                                <th align="center">Type De Financement</th>
                                <th align="center">AutoFinancement</th>
                                <th align="center">Prix</th>
                                <th align="center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($members as $key => $item)
                                <tr>
                                    <td align="center">{{ $item->id }}</td>
                                    <td align="center">{{ $item->name }} {{ $item->surname }}</td>
                                    <td align="center">{{ $item->email }}</td>
                                    <td align="center">{{ $item->telephone }}</td>
                                    <td align="center">{{ $item->financement }}</td>
                                    <td align="center">{{ $item->auto_financement }} &nbsp; DT</td>
                                    <td align="center">{{ $item->prix }} &nbsp; DT</td>
                                    <td align="center">
                                        <a href="{{ action('Admin\AdminController@destroy', ['id' => $item->id]) }}" class="fa fa-trash"></a>
                                        <a href="{{ action('Admin\AdminController@edit', ['id' => $item->id]) }}" class="fa fa-pencil-square-o"></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

</div>
<!-- /page content -->
@push('scripts')

        <!-- bootstrap-daterangepicker -->
<script>
    $(document).ready(function() {

        var cb = function(start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };

        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            var ds = $('.daterangepicker_input input[name="daterangepicker_start"]').val();
            var de = $('.daterangepicker_input input[name="daterangepicker_end"]').val();
            var mon = ds.substr(0,2);
            var day = ds.substr(3,2);
            var year = ds.substr(6,4);
            ds1 = year+"-"+mon+"-"+day;
            mon = de.substr(0,2);
            day = de.substr(3,2);
            year = de.substr(6,4);
            var de1 = year+"-"+mon+"-"+day;
            $('.startdate').value = ds1;
            $('.enddate').value = de1;
            var date = ds1+"/"+de1;
            window.location = "{{ URL::to('admin/print') }}/"+date;
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
            console.log("cancel event fired");
        });
        $('#options1').click(function() {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>
<!-- /bootstrap-daterangepicker -->

<script type="text/javascript">
    $(document).ready(function() {
        var handleDataTableButtons = function() {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true
                });
            }
        };

        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        TableManageButtons.init();
        $('.selectpicker option[value="{{Request::segment(1).'/'.Request::segment(2)}}"]').attr('selected','true');
    });
</script>

@endpush
@endsection