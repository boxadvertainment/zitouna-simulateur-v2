@extends('admin.layouts.blank')

@section('main_container')

        <!-- page content -->
<div class="right_col" role="main">

    <div class="x_content">

        <div class="loading"></div>

        <form class="form-horizontal form-label-left" id="form" name="form" action="{{ action('Admin\AdminController@update', ['id' => $member->id]) }}" method="POST" novalidate enctype="multipart/form-data">

            {!! csrf_field() !!}

            <p>Exemple Form Validation <code>parsleyJS</code></p>

            <span class="section">Information</span>

            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name_surname">Name <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="name" name="name" class="form-control col-md-7 col-xs-12" placeholder="Exemple : Lorem Lorem" required="required" type="text" value="{{ $member->name }}">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name_surname">Name <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="surname" name="surname" class="form-control col-md-7 col-xs-12" placeholder="Exemple : Lorem Lorem" required="required" type="text" value="{{ $member->surname }}">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="email" id="email" name="email" required="required" placeholder="EX : yourname@exemple.com" class="form-control col-md-7 col-xs-12" value="{{ $member->email }}">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Téléphone <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="tel" name="telephone" id="telephone" required="required" data-validate-length="8" class="form-control col-md-7 col-xs-12" maxlength="8" value="{{ $member->telephone }}">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lien">Lien <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="lien" id="lien" value="{{$member->lien}}" class="form-control col-md-7 col-xs-12" placeholder="Exemple : Lorem Lorem" required="required" type="text">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prix">Prix <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="prix" id="prix" value="{{$member->prix}}" class="form-control col-md-7 col-xs-12" required="required">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="duree">Durée <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input name="duree" id="duree" class="form-control" value="{{$member->duree}}" class="form-control col-md-7 col-xs-12" required="required" type="text">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="age">Age <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input name="age" id="age" value="{{$member->age}}" class="form-control col-md-7 col-xs-12" required="required" type="text">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="puissance_fiscale">Puissance Fiscale <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input name="puissance_fiscale" id="puissance_fiscale" value="{{$member->puissance_fiscale}}" class="form-control col-md-7 col-xs-12" required="required" type="text">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="financement">Financement <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input name="financement" id="financement" value="{{$member->financement}}" class="form-control col-md-7 col-xs-12" required="required" type="text">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="auto_financement">Auto Financement <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input name="auto_financement" id="auto_financement" value="{{$member->auto_financement}}"class="form-control col-md-7 col-xs-12" required="required" type="text">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="message">Status <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select id="status" class="form-control" name="status" required="required">
                        <option value="Pending"  <?php if ($member->status == 'Pending') echo 'selected'; ?> >Pending</option>
                        <option value="Accepted" <?php if ($member->status == 'Accepted') echo 'selected'; ?>>Accepted</option>
                        <option value="Rejected" <?php if ($member->status == 'Rejected') echo 'selected'; ?>>Rejected</option>
                    </select>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <button type="reset" class="btn btn-primary">Cancel</button>
                    <button id="send" type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>

</div>
<!-- /page content -->
@push('scripts')

<script type="text/javascript">

    $('.loading').hide();

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
    });

    $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit){
            $('.loading').show();
            $.ajax({
                url: $('#form').attr('action'),
                method: 'POST',
                data: $('#form').serialize(),
                statusCode: {
                    500: function() {
                        $('.loading').hide();
                        return swal('Oups!', 'Une erreur s\'est produite, veuillez réessayer ultérieurement.', 'error');
                    }
                }
            }).done(function(response){
                $('.loading').hide();
                if(response.success) {
                    return swal("Success!", response.message, "success")
                }
                return swal('Oups!', response.message, 'error');
            });
        }
        return false;
    });
</script>

@endpush
@endsection


