@extends('errors.layout')

@section('main_container')
        <!-- page content -->
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <img src="{{ asset('images/robox-logo.png') }}" alt="Robox">
                    <h1 class="error-number">403</h1>
                    <h2>Access denied</h2>
                    <p>Full authentication is required to access this resource.</p>
                </div>
            </div>
        </div>
        <!-- /page content -->
@endsection

