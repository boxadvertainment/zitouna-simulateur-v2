@extends('errors.layout')

@section('main_container')
        <!-- page content -->
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <img src="{{ asset('images/robox-logo.png') }}" alt="Robox">
                    <h1 class="error-number">404</h1>
                    <h2>Sorry but we couldn't find this page</h2>
                    <p>This page you are looking for does not exist.</p>
                </div>
            </div>
        </div>
        <!-- /page content -->

@endsection

