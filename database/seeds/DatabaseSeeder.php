<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder{

    public function run(){
        //$this->call('UsersTableSeeder');
        DB::table('users')->delete();
        User::create([
            'full_name' => 'box agency',
            'username' => 'admin',
            'email' => 'admin@box.agency',
            'password' => bcrypt('admin@2016')
        ]);
    }
}
