<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration{
    public function up(){
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_surname');
            $table->string('phone');
            //$table->string('number');
            $table->string('email')->unique();
            //$table->string('password');
            $table->string('status');
            $table->string('photo');
            //$table->string('website');
            //$table->string('hobbies')->nullable();
            $table->text('message');
            //$table->string('heard');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::drop('participants');
    }
}
