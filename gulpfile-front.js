var elixir = require('laravel-elixir');
require('./elixir-extensions');

elixir(function(mix) {

    mix
        .sass('main.scss')
        .babel([
            'facebookUtils.js',
            'main.js'
        ], 'public/js/main.js')

        /*******************     scripts front End start here          ******************/
        .scripts([
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
        ], 'public/js/vendor-front.js', 'bower_components')
        .scripts([
            // bower:js
            'bower_components/sweetAlert2/dist/sweetalert2.js',
            // endbower
        ], 'public/js/plugins-front.js', 'bower_components')
        /*******************     styles front End start here          ******************/
        .styles([
            // bower:css
            'bower_components/animate.css/animate.css',
            'bower_components/font-awesome/css/font-awesome.css',
            'bower_components/sweetAlert2/dist/sweetalert2.css',
            // endbower
        ], 'public/css/plugins-front.css', 'bower_components')


});
