<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Participant;
use App\Member;

class AppController extends Controller{

    public function index(){
        return view('index');
    }
    public function createSimulation(){
        return view('create');
    }

    public function store(Request $request){

        $validator = \Validator::make($request->all(), [
            'name'  =>  'required',
            'surname'  =>  'required',
            'email' =>  'required|email',
            'telephone' =>  'required|regex:/^[0-9]{8}$/',
            'prix'  =>  'required',
            'auto_financement'  =>  'required',
            'puissance_fiscale'  =>  'required|integer|between:4,9',
            'financement'  =>  'required',
        ]);
        //
        // process the login
        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all()) ]);
        } else {
            $member = new Member();
            $member->name = $request->name;
            $member->surname = $request->surname;
            $member->email = $request->email;
            $member->telephone = $request->telephone;
            $member->prix = $request->prix;
            $member->lien = $_SERVER['HTTP_REFERER'];
            $member->auto_financement = $request->auto_financement;
            $member->duree = $request->duree;
            $member->financement = $request->financement;
            if ($request->financement == 'Tamouil_Sayara'){
                $member->puissance_fiscale = $request->puissance_fiscale;
                $member->age = $request->age;
            }else{
                $member->puissance_fiscale = null;
                $member->age = null;
            }
            $member->save();
            return response()->json(['success' => true]);
        }
    }

}
