<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Member;

class AdminController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function dashboard(){
        $sayara = 0;
        $nbsayara = 0;
        $ennakl = 0;
        $nbennakl = 0;
        $mochtarayet = 0;
        $nbmochtarayet = 0;
        $menzel = 0;
        $nbmanzel = 0;
        $akkarat = 0;
        $nbakkarat = 0;

        $total= Member::all()->count();
        if($total != 0) {
            $nbsayara = Member::where('financement', '=', 'Tamouil_Sayara')->count();
            $sayara = (100 / $total) * $nbsayara;
            $nbennakl = Member::where('financement', '=', 'Tamouil_Moueddet_Ennakl')->count();
            $ennakl = (100 / $total) * $nbennakl;
            $nbmochtarayet = Member::where('financement', '=', 'Tamouil_Mochtarayet')->count();
            $mochtarayet = (100 / $total) * $nbmochtarayet;
            $nbmanzel = Member::where('financement', '=', 'Tamouil_Menzel')->count();
            $menzel = (100 / $total) * $nbmanzel;
            $nbakkarat = Member::where('financement', '=', 'Tamouil_Akkarat_Afrad')->count();
            $akkarat = (100 / $total) * $nbakkarat;
        }

        //financement par Montant
        $montant = Member::sum('prix');
        $nbs = Member::where('financement', '=', 'Tamouil_Sayara')->sum('prix');
        if($nbs != 0) {
            $plage1 = (100 / ($montant / $nbs));
        }else{
            $plage1 = 0;
        }
        $nbnk = Member::where('financement', '=', 'Tamouil_Moueddet_Ennakl')->sum('prix');
        if($nbnk != 0) {
            $plage2 = (100 / ($montant / $nbnk));
        }else{
            $plage2 = 0;
        }
        $nbmch = Member::where('financement', '=', 'Tamouil_Mochtarayet')->sum('prix');
        if($nbmch != 0) {
            $plage3 = (100 / ($montant / $nbmch));
        }else{
            $plage3 = 0;
        }
        $nbmz = Member::where('financement', '=', 'Tamouil_Menzel')->sum('prix');
        if($nbmz != 0){
            $plage4 = (100 / ($montant/$nbmz));
        }else{
            $plage4 = 0;
        }

        $nbakk = Member::where('financement', '=', 'Tamouil_Akkarat_Afrad')->sum('prix');
        if($nbakk != 0){
            $plage5 = (100 / ($montant/$nbakk));
        }else{
            $plage5 = 0;
        }

        return view('admin.dashboard')->with([
            'nbsayara' => $nbsayara,
            'nbennakl' => $nbennakl,
            'nbmochtarayet' => $nbmochtarayet,
            'nbmanzel' => $nbmanzel,
            'nbakkarat' => $nbakkarat,
            'total' => $total,
            'sayara' => $sayara,
            'ennakl' => $ennakl,
            'mochtarayet' => $mochtarayet,
            'menzel' => $menzel,
            'akkarat' => $akkarat,
            'montant' => $montant,
            'plage1' => $plage1,
            'plage2' => $plage2,
            'plage3' => $plage3,
            'plage4' => $plage4,
            'plage5' => $plage5
        ]);
    }
    public function form(){
        return view('admin.create');
    }
    public function pagination(){
        $type_category = "all";
        $members = Member::all();
        return view('admin.pagination')->with([
            'members'=> $members,
            'type_category' => $type_category
        ]);
    }
    public function edit($id){
        $member = Member::find($id);
        return view('admin.Edit')->with([
            'member' => $member
        ]);
    }
    public function update($id, Request $request){
        $messages = [
            'required' => 'The :attribute field is required.',
            'email' => 'The :attribute must be an  email.',
            'integer' => 'The :attribute must be an integer.',
        ];
        $validator = \Validator::make($request->all(), [
            'name'  =>  'required',
            'surname'  =>  'required',
            'email' =>  'required|email',
            'telephone' =>  'required|regex:/^[0-9]{8}$/',
            'prix'  =>  'required',
            'lien'  =>  'required|url',
            'status'  =>  'required',
            'auto_financement'  =>  'required',
            'duree'  =>  'required|integer|between:1,7',
            'puissance_fiscale'  =>  'required|integer|between:4,9',
            'age'  =>  'required|integer|between:0,3',
            'financement'  =>  'required',
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all())]);
        } else {
            $member = Member::find($id);
            $member->name       = $request->name;
            $member->surname       = $request->surname;
            $member->email      = $request->email;
            $member->telephone = $request->telephone;
            $member->prix = $request->prix;
            $member->lien = $request->lien;
            $member->status = $request->status;
            $member->auto_financement = $request->auto_financement;
            $member->duree = $request->duree;
            $member->puissance_fiscale = $request->puissance_fiscale;
            $member->age = $request->age;
            $member->financement = $request->financement;
            if ($member->save()){
                return response()->json(['success' => true, 'message' => 'Modification fait avec success']);
            }
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);

        }
    }
    public function destroy($id){
        $member = Member::find($id);
        $member->delete();

        return view('admin.pagination')->with([
            'members' => Member::all()
        ]);
    }

    public function tamouil_sayara(){
        $type_category = "sayara";
        $members = Member::where('financement', '=', 'Tamouil_Sayara')->get();
        return view('admin.pagination_sayara')->with([
            'members' => $members,
            'type_category' => $type_category
        ]);
    }
    public function tamouil_akkarat_afrad(){
        $type_category = "akkarat afrad";
        $members = Member::where('financement', '=', 'Tamouil_Akkarat_Afrad')->get();
        return view('admin.pagination_akkarat_afrad')->with([
            'members' => $members,
            'type_category' => $type_category
        ]);
    }
    public function tamouil_moueddet_ennakl(){
        $type_category = "moueddet ennakl";
        $members = Member::where('financement', '=', 'Tamouil_Moueddet_Ennakl')->get();
        return view('admin.pagination_ennakl')->with([
            'members' => $members,
            'type_category' => $type_category
        ]);
    }
    public function tamouil_mochtarayet(){
        $type_category = "mochtarayet";
        $members = Member::where('financement', '=', 'Tamouil_Mochtarayet')->get();
        return view('admin.pagination_mochtarayet')->with([
            'members' => $members,
            'type_category' => $type_category
        ]);
    }
    public function tamouil_menzel(){
        $type_category = "menzel";
        $members = Member::where('financement', '=', 'Tamouil_Menzel')->get();
        return view('admin.pagination_menzel')->with([
            'members' => $members,
            'type_category' => $type_category
        ]);
    }

    public function printcsv($dateDebut, $dateFin){
        $dateDebut = $dateDebut." 00:00:00";
        $dateFin = $dateFin." 23:59:59";
        $members = Member::whereBetween('created_at', array($dateDebut, $dateFin))->get();
        return view('admin.pagination_csv')->with('members', $members);
    }
}
