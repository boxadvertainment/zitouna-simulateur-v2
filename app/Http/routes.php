<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'AppController@index');
Route::get('/create', 'AppController@createSimulation');
Route::post('/add', 'AppController@store');
// Route::post('auth/facebookLogin/{offline?}', 'Auth\AuthController@facebookLogin');
// Route::post('signup', 'AppController@signup');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {
    Route::auth();
    Route::get('/', 'AdminController@dashboard');
    //Route::get('/form', 'AdminController@form');
    Route::get('/list', 'AdminController@pagination');
    Route::get('/tamouil_sayara', 'AdminController@tamouil_sayara');
    Route::get('/tamouil_moueddet_ennakl', 'AdminController@tamouil_moueddet_ennakl');
    Route::get('/tamouil_akkarat_afrad', 'AdminController@tamouil_akkarat_afrad');
    Route::get('/tamouil_mochtarayet', 'AdminController@tamouil_mochtarayet');
    Route::get('/tamouil_menzel', 'AdminController@tamouil_menzel');

    Route::get('/print/{date_deb}/{date_fin}', 'AdminController@printcsv');

    //Route::post('/store', 'ParticipantController@store');
    Route::get('/show/{id}', 'AdminController@edit');
    Route::post('/update/{id}', 'AdminController@update');
    Route::get('/delete/{id}', 'AdminController@destroy');
});