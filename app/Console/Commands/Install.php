<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use DB;
use Schema;
use App\User;
use Symfony\Component\Console\Helper\SymfonyQuestionHelper;
use Symfony\Component\Console\Question\Question;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'robox:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installation of Robox-Sys: Laravel setup, installation of composer and npm packages';

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;
    /**
     * The user model.
     *
     * @var \App\User
     */
    protected $user;
    /**
     * Create a new command.
     *
     * @param \App\User                              $user
     * @param \Illuminate\Filesystem\Filesystem     $files
     */
    public function __construct(User $user, Filesystem $files)
    {
        parent::__construct();
        $this->user = $user;
        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('---------------------------------');
        $this->line('Welcome to Robox-Sys installation');
        $this->line('---------------------------------');

        // Ask for porject URL
        $contents = $this->getKeyFile();
        $APP_URL = $this->ask('What is the project URL', $this->guessDatabaseName().'.dev');

        // Update APP_URL in .env file.
        $search = [
            '/('.preg_quote('APP_URL=').')(.*)/'
        ];
        $replace = [
            '$1'.$APP_URL
        ];
        $contents = preg_replace($search, $replace, $contents);
        if (!$contents) {
            throw new Exception('Error while writing credentials to .env file.');
        }
        // Write to .env
        $this->files->put('.env', $contents);

        // Writing the serve command with the APP_URL
        $this->line('Run this command in the homestead console : ');
        $this->comment(' serve '.$APP_URL.' /home/vagrant/Code/'.$this->guessDatabaseName().'/public');
        $this->line('');

        // Ask for database name
        $this->info('Setting up database...');
        $dbName = $this->ask('Enter a database name', $this->guessDatabaseName());

        // Set database credentials in .env and migrate
        //$this->call('robox:database', ['database' => $dbName]);
        $this->setupDatabase($dbName);
        $this->line('-----------------------------------');

        // Create a super user
        $this->createSuperUser();

        // Set cache key prefix
        $this->call('cache:prefix', ['prefix' => $dbName]);
        $this->line('-----------------------------------');

        // Composer install
        if (function_exists('system')) {
            system('find storage -type d -exec chmod 755 {} \;');
            $this->info('Directory storage is now writable (755).');
            system('find bootstrap/cache -type d -exec chmod 755 {} \;');
            $this->info('Directory bootstrap/cache is now writable (755).');
//            system('find public/uploads -type d -exec chmod 755 {} \;');
//            $this->info('Directory public/uploads is now writable (755).');
            //system('find public/html -type d -exec chmod 755 {} \;');
            //$this->info('Directory public/html is now writable (755).');
            $this->line('-----------------------------------');
            $this->info('Running npm install...');
            system('npm install');
            $this->info('npm packages installed.');
        } else {
            $this->line('You can now make /storage, /bootstrap/cache and /public/uploads directories writable.');
            $this->line('and run composer install and npm install.');
        }
    }

    /**
     * Guess database name from app folder.
     *
     * @return string
     */
    public function guessDatabaseName()
    {
        try {
            $segments = array_reverse(explode(DIRECTORY_SEPARATOR, app_path()));
            $name = explode('.', $segments[1])[0];
            return str_slug($name);
        } catch (Exception $e) {
            return '';
        }
    }


    private function setupDatabase($dbName){
        $contents = $this->getKeyFile();
        $dbUserName = $this->ask('What is your MySQL username', 'homestead');
        $question = new Question('What is your MySQL password', 'secret');
        $question->setHidden(true)->setHiddenFallback(true);
        $dbPassword = (new SymfonyQuestionHelper())->ask($this->input, $this->output, $question);
        if ($dbPassword === 'none') {
            $dbPassword = '';
        }
        // Update DB credentials in .env file.
        $search = [
            '/('.preg_quote('DB_DATABASE=').')(.*)/',
            '/('.preg_quote('DB_USERNAME=').')(.*)/',
            '/('.preg_quote('DB_PASSWORD=').')(.*)/',
        ];
        $replace = [
            '$1'.$dbName,
            '$1'.$dbUserName,
            '$1'.$dbPassword,
        ];
        $contents = preg_replace($search, $replace, $contents);
        if (!$contents) {
            throw new Exception('Error while writing credentials to .env file.');
        }
        // Set DB username and password in config
        $this->laravel['config']['database.connections.mysql.username'] = $dbUserName;
        $this->laravel['config']['database.connections.mysql.password'] = $dbPassword;
        // Clear DB name in config
        unset($this->laravel['config']['database.connections.mysql.database']);
        // Force the new login to be used
        DB::purge();
        // Create database if not exists
        DB::unprepared('CREATE DATABASE IF NOT EXISTS `'.$dbName.'`');
        DB::unprepared('USE `'.$dbName.'`');
        DB::connection()->setDatabaseName($dbName);
        // Migrate DB
        if (Schema::hasTable('migrations')) {
            $this->error('A migrations table was found in database ['.$dbName.'], no migration and seed were done.');
        } else {
            $this->call('migrate');
            $this->call('db:seed');
        }
        // Write to .env
        $this->files->put('.env', $contents);
    }


    /**
     * Create a superuser.
     */
    private function createSuperUser()
    {
        $this->info('Creating a Super Admin...');
        $this->user = New User();
        $this->user->full_name  = $this->ask('Enter your full name');
        $this->user->username   = $this->ask('Enter your username to login', 'admin');
        $this->user->email      = $this->ask('Enter your email address', 'admin@box.agency');
        $this->user->password   = bcrypt( $this->secret('Enter a password') );

//        $user->full_name =
//            'full_name'   => $full_name,
//            'username'    => $username,
//            'email'       => $email,
//            'password'    => bcrypt($password)
//        ];

        try {
            $this->user->save();
            $this->info('Super Admin created.');
        } catch (Exception $e) {
            $this->error('Admin could not be created :(.' . $e );
        }
        $this->line('------------------');
    }


    /**
     * Get the key file and return its content.
     *
     * @return string
     */
    protected function getKeyFile()
    {
        return $this->files->exists('.env') ? $this->files->get('.env') : $this->files->get('.env.example');
    }
}
