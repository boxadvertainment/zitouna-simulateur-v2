<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    //
    public function getFullNameAttribute(){
        return $this->attributes['name'].' '.$this->attributes['surname'] ;
    }
}
